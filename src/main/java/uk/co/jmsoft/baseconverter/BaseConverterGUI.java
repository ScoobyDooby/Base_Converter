/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.baseconverter;

import ZoeloeSoft.projects.JFontChooser.JFontChooser;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import uk.co.jmsoft.baseconversiontools.BaseConversionTools;
import uk.co.jmsoft.stringformatter.StringFormatter;

/**
 *
 * @author Scooby
 */
public class BaseConverterGUI extends javax.swing.JFrame
{

    private final String appName = "Base Converter";
    private final String version = "1.0";
    private JFontChooser jFontChooser = null;
    private final Preferences preferences;
    DocumentListener binaryTextFieldListener;
    DocumentListener octalTextFieldListener;
    DocumentListener decimalTextFieldListener;
    DocumentListener hexTextFieldListener;

    private String getAppTitle()
    {
        return appName + " " + version;
    }

    public static void main(String... args)
    {
        BaseConverterGUI baseConverterGUI = new BaseConverterGUI();
        baseConverterGUI.setVisible(true);
    }

    int binarySpacing = 4;
    Base textBoxWithFocus;

    private class MyDocumentListener implements DocumentListener
    {

        @Override
        public void insertUpdate(DocumentEvent de)
        {
            removeDocListeners();
            onInputChange();
            addDocListeners();
        }

        @Override
        public void removeUpdate(DocumentEvent de)
        {
            removeDocListeners();
            onInputChange();
            addDocListeners();
        }

        @Override
        public void changedUpdate(DocumentEvent de)
        {
            removeDocListeners();
            onInputChange();
            addDocListeners();
        }
    }

    /**
     * Creates new form BaseConverterGUI
     */
    public BaseConverterGUI()
    {
        initComponents();

        preferences = Preferences.userNodeForPackage(this.getClass());

        this.setSize(preferences.getInt("Width", 200), preferences.getInt("Height", 300));

        binaryPanel.setVisible(preferences.getBoolean("ShowBinary", true));
        octalPanel.setVisible(preferences.getBoolean("ShowOctal", true));
        decimalPanel.setVisible(preferences.getBoolean("ShowDecimal", true));
        hexPanel.setVisible(preferences.getBoolean("ShowHex", true));

        showBinaryCheckBoxMenuItem.setSelected(preferences.getBoolean("ShowBinary", true));
        showOctalCheckBoxMenuItem.setSelected(preferences.getBoolean("ShowOctal", true));
        showDecimalCheckBoxMenuItem.setSelected(preferences.getBoolean("ShowDecimal", true));
        showHexCheckBoxMenuItem.setSelected(preferences.getBoolean("ShowHex", true));
        //Set font preference
        Font font = new Font(preferences.get("FontName", "Tahoma"), preferences.getInt("FontStyle", Font.PLAIN), preferences.getInt("FontPointSize", 13));
        binaryTextField.setFont(font);
        octalTextField.setFont(font);
        decimalTextField.setFont(font);
        hexTextField.setFont(font);

        binaryTextField.requestFocus();
        binaryTextFieldListener = new MyDocumentListener();
        octalTextFieldListener = new MyDocumentListener();
        decimalTextFieldListener = new MyDocumentListener();
        hexTextFieldListener = new MyDocumentListener();

        binaryTextField.getDocument().addDocumentListener(binaryTextFieldListener);
        octalTextField.getDocument().addDocumentListener(octalTextFieldListener);
        decimalTextField.getDocument().addDocumentListener(decimalTextFieldListener);
        hexTextField.getDocument().addDocumentListener(hexTextFieldListener);
        
        
        setLocation(preferences.getInt("xLocation", getScreenCenterX()), preferences.getInt("yLocation", getScreenCenterY()));
    }
    
    private int getScreenCenterX()
    {
        return Toolkit.getDefaultToolkit().getScreenSize().width/2 - getWidth()/2;
    }
    private int getScreenCenterY()
    {
        return Toolkit.getDefaultToolkit().getScreenSize().height/2 - getHeight()/2;
    }

    private void packVertical()
    {
        int width = getWidth();
        pack();
        setSize(width, getHeight());
    }

    private void addDocListeners()
    {
        binaryTextField.getDocument().addDocumentListener(binaryTextFieldListener);
        octalTextField.getDocument().addDocumentListener(octalTextFieldListener);
        decimalTextField.getDocument().addDocumentListener(decimalTextFieldListener);
        hexTextField.getDocument().addDocumentListener(hexTextFieldListener);
    }

    private void removeDocListeners()
    {
        binaryTextField.getDocument().removeDocumentListener(binaryTextFieldListener);
        octalTextField.getDocument().removeDocumentListener(octalTextFieldListener);
        decimalTextField.getDocument().removeDocumentListener(decimalTextFieldListener);
        hexTextField.getDocument().removeDocumentListener(hexTextFieldListener);
    }

    /**
     * This method is called from within the constructor to
     * initialise the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        helpFrame = new javax.swing.JFrame();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jMenuItem2 = new javax.swing.JMenuItem();
        layoutPanel = new javax.swing.JPanel();
        binaryPanel = new javax.swing.JPanel();
        binaryTextField = new javax.swing.JTextField();
        octalPanel = new javax.swing.JPanel();
        octalTextField = new javax.swing.JTextField();
        decimalPanel = new javax.swing.JPanel();
        decimalTextField = new javax.swing.JTextField();
        hexPanel = new javax.swing.JPanel();
        hexTextField = new javax.swing.JTextField();
        jMenuBar1 = new javax.swing.JMenuBar();
        optionsMenu = new javax.swing.JMenu();
        pinCheckBoxMenuItem = new javax.swing.JCheckBoxMenuItem();
        showBinaryCheckBoxMenuItem = new javax.swing.JCheckBoxMenuItem();
        showOctalCheckBoxMenuItem = new javax.swing.JCheckBoxMenuItem();
        showDecimalCheckBoxMenuItem = new javax.swing.JCheckBoxMenuItem();
        showHexCheckBoxMenuItem = new javax.swing.JCheckBoxMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        fontMenuItem = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        resetPreferencesMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();

        helpFrame.setTitle("Help - " + getAppTitle());
        helpFrame.setResizable(false);

        jLabel1.setText("Underscores can be used as spaces when entering numbers. eg. 1000_1010");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel2.setText("Entering numbers");

        javax.swing.GroupLayout helpFrameLayout = new javax.swing.GroupLayout(helpFrame.getContentPane());
        helpFrame.getContentPane().setLayout(helpFrameLayout);
        helpFrameLayout.setHorizontalGroup(
            helpFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(helpFrameLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(helpFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(helpFrameLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel1))
                    .addComponent(jLabel2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        helpFrameLayout.setVerticalGroup(
            helpFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(helpFrameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addContainerGap(149, Short.MAX_VALUE))
        );

        helpFrame.getAccessibleContext().setAccessibleName("Help - " + getAppTitle());
        helpFrame.getAccessibleContext().setAccessibleDescription("The help file for " + getAppTitle());

        jMenuItem2.setText("Add base");
        jMenuItem2.getAccessibleContext().setAccessibleDescription("Add another base to be calculated.");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle(getAppTitle());
        setAlwaysOnTop(true);
        setName("textBoxFrame"); // NOI18N
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });

        binaryPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("binary"));
        binaryPanel.setMinimumSize(decimalTextField.getMinimumSize());
        binaryPanel.setPreferredSize(new java.awt.Dimension(378, 60));

        binaryTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        binaryTextField.setMinimumSize(new java.awt.Dimension(30, 22));
        binaryTextField.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusGained(java.awt.event.FocusEvent evt)
            {
                binaryTextFieldFocusGained(evt);
            }
        });
        binaryTextField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                binaryTextFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout binaryPanelLayout = new javax.swing.GroupLayout(binaryPanel);
        binaryPanel.setLayout(binaryPanelLayout);
        binaryPanelLayout.setHorizontalGroup(
            binaryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(binaryTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        binaryPanelLayout.setVerticalGroup(
            binaryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(binaryTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        octalPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("octal"));
        octalPanel.setMinimumSize(decimalTextField.getMinimumSize());
        octalPanel.setPreferredSize(new java.awt.Dimension(378, 60));

        octalTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        octalTextField.setMinimumSize(new java.awt.Dimension(30, 22));
        octalTextField.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusGained(java.awt.event.FocusEvent evt)
            {
                octalTextFieldFocusGained(evt);
            }
        });
        octalTextField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                octalTextFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout octalPanelLayout = new javax.swing.GroupLayout(octalPanel);
        octalPanel.setLayout(octalPanelLayout);
        octalPanelLayout.setHorizontalGroup(
            octalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(octalTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        octalPanelLayout.setVerticalGroup(
            octalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(octalTextField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        decimalPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("decimal"));
        decimalPanel.setMinimumSize(decimalTextField.getMinimumSize());
        decimalPanel.setPreferredSize(new java.awt.Dimension(378, 60));

        decimalTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        decimalTextField.setMinimumSize(new java.awt.Dimension(30, 22));
        decimalTextField.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusGained(java.awt.event.FocusEvent evt)
            {
                decimalTextFieldFocusGained(evt);
            }
        });
        decimalTextField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                decimalTextFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout decimalPanelLayout = new javax.swing.GroupLayout(decimalPanel);
        decimalPanel.setLayout(decimalPanelLayout);
        decimalPanelLayout.setHorizontalGroup(
            decimalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(decimalTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        decimalPanelLayout.setVerticalGroup(
            decimalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(decimalTextField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        hexPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("hexadecimal"));
        hexPanel.setMinimumSize(decimalTextField.getMinimumSize());
        hexPanel.setPreferredSize(new java.awt.Dimension(378, 60));

        hexTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        hexTextField.setMinimumSize(new java.awt.Dimension(30, 22));
        hexTextField.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusGained(java.awt.event.FocusEvent evt)
            {
                hexTextFieldFocusGained(evt);
            }
        });
        hexTextField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                hexTextFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout hexPanelLayout = new javax.swing.GroupLayout(hexPanel);
        hexPanel.setLayout(hexPanelLayout);
        hexPanelLayout.setHorizontalGroup(
            hexPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(hexTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        hexPanelLayout.setVerticalGroup(
            hexPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(hexTextField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layoutPanelLayout = new javax.swing.GroupLayout(layoutPanel);
        layoutPanel.setLayout(layoutPanelLayout);
        layoutPanelLayout.setHorizontalGroup(
            layoutPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(binaryPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(octalPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(decimalPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(hexPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layoutPanelLayout.setVerticalGroup(
            layoutPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layoutPanelLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(binaryPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(octalPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(decimalPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(hexPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        optionsMenu.setMnemonic('S');
        optionsMenu.setText("Settings");

        pinCheckBoxMenuItem.setMnemonic('A');
        pinCheckBoxMenuItem.setSelected(true);
        pinCheckBoxMenuItem.setText("Always on top");
        pinCheckBoxMenuItem.setIcon(new javax.swing.ImageIcon("C:\\Users\\Scooby\\Documents\\Programming\\NetBeansProjects\\BaseConverter\\pinIcon.png")); // NOI18N
        pinCheckBoxMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                pinCheckBoxMenuItemActionPerformed(evt);
            }
        });
        optionsMenu.add(pinCheckBoxMenuItem);

        showBinaryCheckBoxMenuItem.setSelected(true);
        showBinaryCheckBoxMenuItem.setText("Show binary");
        showBinaryCheckBoxMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                showBinaryCheckBoxMenuItemActionPerformed(evt);
            }
        });
        optionsMenu.add(showBinaryCheckBoxMenuItem);

        showOctalCheckBoxMenuItem.setSelected(true);
        showOctalCheckBoxMenuItem.setText("Show octal");
        showOctalCheckBoxMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                showOctalCheckBoxMenuItemActionPerformed(evt);
            }
        });
        optionsMenu.add(showOctalCheckBoxMenuItem);

        showDecimalCheckBoxMenuItem.setSelected(true);
        showDecimalCheckBoxMenuItem.setText("Show decimal");
        showDecimalCheckBoxMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                showDecimalCheckBoxMenuItemActionPerformed(evt);
            }
        });
        optionsMenu.add(showDecimalCheckBoxMenuItem);

        showHexCheckBoxMenuItem.setSelected(true);
        showHexCheckBoxMenuItem.setText("Show hexadecimal");
        showHexCheckBoxMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                showHexCheckBoxMenuItemActionPerformed(evt);
            }
        });
        optionsMenu.add(showHexCheckBoxMenuItem);
        optionsMenu.add(jSeparator1);

        fontMenuItem.setMnemonic('F');
        fontMenuItem.setText("Font...");
        fontMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                fontMenuItemActionPerformed(evt);
            }
        });
        optionsMenu.add(fontMenuItem);
        fontMenuItem.getAccessibleContext().setAccessibleName("Font dialog");
        fontMenuItem.getAccessibleContext().setAccessibleDescription("");

        optionsMenu.add(jSeparator2);

        resetPreferencesMenuItem.setText("Reset user preferences");
        resetPreferencesMenuItem.setToolTipText("Resets all preferences such as font, window size etc. to default values.");
        resetPreferencesMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                resetPreferencesMenuItemActionPerformed(evt);
            }
        });
        optionsMenu.add(resetPreferencesMenuItem);

        jMenuBar1.add(optionsMenu);

        helpMenu.setMnemonic('H');
        helpMenu.setText("Help...");
        helpMenu.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                helpMenuMouseClicked(evt);
            }
        });
        jMenuBar1.add(helpMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(layoutPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(layoutPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        getAccessibleContext().setAccessibleName(getAppTitle());
        getAccessibleContext().setAccessibleDescription("An application for converting numbers between different bases.");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void onInputChange()
    {
        if (textBoxWithFocus != Base.BINARY)
        {
            binaryTextField.setText("");
        }
        if (textBoxWithFocus != Base.OCTAL)
        {
            octalTextField.setText("");
        }
        if (textBoxWithFocus != Base.DECIMAL)
        {
            decimalTextField.setText("");
        }
        if (textBoxWithFocus != Base.HEXADECIMAL)
        {
            hexTextField.setText("");
        }
    }


    private void helpMenuMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_helpMenuMouseClicked
    {//GEN-HEADEREND:event_helpMenuMouseClicked
        // Make sure location is relative to the main frame.
        Point location = this.getLocation();
        helpFrame.setLocation(location.x + 40, location.y + 40);

        // Make sure the dialog always appears on top of the main frame.
        helpFrame.setAlwaysOnTop(this.isAlwaysOnTop());
        helpFrame.setVisible(true);
    }//GEN-LAST:event_helpMenuMouseClicked

    private void pinCheckBoxMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_pinCheckBoxMenuItemActionPerformed
    {//GEN-HEADEREND:event_pinCheckBoxMenuItemActionPerformed
        if (pinCheckBoxMenuItem.isSelected())
        {
            this.setAlwaysOnTop(true);
        }
        else
        {
            this.setAlwaysOnTop(false);
        }
    }//GEN-LAST:event_pinCheckBoxMenuItemActionPerformed

    private void hexTextFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_hexTextFieldActionPerformed
    {//GEN-HEADEREND:event_hexTextFieldActionPerformed
        updateNumbers();
    }//GEN-LAST:event_hexTextFieldActionPerformed

    private void decimalTextFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_decimalTextFieldActionPerformed
    {//GEN-HEADEREND:event_decimalTextFieldActionPerformed
        updateNumbers();
    }//GEN-LAST:event_decimalTextFieldActionPerformed

    private void octalTextFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_octalTextFieldActionPerformed
    {//GEN-HEADEREND:event_octalTextFieldActionPerformed
        updateNumbers();
    }//GEN-LAST:event_octalTextFieldActionPerformed

    private void binaryTextFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_binaryTextFieldActionPerformed
    {//GEN-HEADEREND:event_binaryTextFieldActionPerformed
        updateNumbers();
    }//GEN-LAST:event_binaryTextFieldActionPerformed

    private void showBinaryCheckBoxMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_showBinaryCheckBoxMenuItemActionPerformed
    {//GEN-HEADEREND:event_showBinaryCheckBoxMenuItemActionPerformed
        binaryPanel.setVisible(showBinaryCheckBoxMenuItem.isSelected());
        packVertical();
    }//GEN-LAST:event_showBinaryCheckBoxMenuItemActionPerformed

    private void showOctalCheckBoxMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_showOctalCheckBoxMenuItemActionPerformed
    {//GEN-HEADEREND:event_showOctalCheckBoxMenuItemActionPerformed
        octalPanel.setVisible(showOctalCheckBoxMenuItem.isSelected());
        packVertical();
    }//GEN-LAST:event_showOctalCheckBoxMenuItemActionPerformed

    private void showDecimalCheckBoxMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_showDecimalCheckBoxMenuItemActionPerformed
    {//GEN-HEADEREND:event_showDecimalCheckBoxMenuItemActionPerformed
        decimalPanel.setVisible(showDecimalCheckBoxMenuItem.isSelected());
        packVertical();
    }//GEN-LAST:event_showDecimalCheckBoxMenuItemActionPerformed

    private void showHexCheckBoxMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_showHexCheckBoxMenuItemActionPerformed
    {//GEN-HEADEREND:event_showHexCheckBoxMenuItemActionPerformed
        hexPanel.setVisible(showHexCheckBoxMenuItem.isSelected());
        packVertical();
    }//GEN-LAST:event_showHexCheckBoxMenuItemActionPerformed

    private void fontMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_fontMenuItemActionPerformed
    {//GEN-HEADEREND:event_fontMenuItemActionPerformed
        if (jFontChooser == null)
        {
            jFontChooser = new JFontChooser(this);
        }
        if (jFontChooser.showDialog(binaryTextField.getFont()) == JFontChooser.OK_OPTION)
        {
            Font font = jFontChooser.getFont();
            binaryTextField.setFont(font);
            decimalTextField.setFont(font);
            octalTextField.setFont(font);
            hexTextField.setFont(font);

            binaryTextField.setMinimumSize(new Dimension(binaryTextField.getMinimumSize().width, font.getSize() + 2));
            octalTextField.setMinimumSize(new Dimension(octalTextField.getMinimumSize().width, font.getSize() + 2));
            decimalTextField.setMinimumSize(new Dimension(decimalTextField.getMinimumSize().width, font.getSize() + 2));
            hexTextField.setMinimumSize(new Dimension(hexTextField.getMinimumSize().width, font.getSize() + 2));

        }
    }//GEN-LAST:event_fontMenuItemActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        preferences.putInt("Width", this.getWidth());
        preferences.putInt("Height", this.getHeight());

        preferences.putBoolean("ShowBinary", binaryPanel.isVisible());
        preferences.putBoolean("ShowOctal", octalPanel.isVisible());
        preferences.putBoolean("ShowDecimal", decimalPanel.isVisible());
        preferences.putBoolean("ShowHex", hexPanel.isVisible());

        Font font = binaryTextField.getFont();
        preferences.put("FontName", font.getFamily());
        preferences.putInt("FontStyle", font.getStyle());
        preferences.putInt("FontPointSize", font.getSize());
        
        preferences.putInt("xLocation", getLocation().x);
        preferences.putInt("yLocation", getLocation().y);
    }//GEN-LAST:event_formWindowClosing

    private void binaryTextFieldFocusGained(java.awt.event.FocusEvent evt)//GEN-FIRST:event_binaryTextFieldFocusGained
    {//GEN-HEADEREND:event_binaryTextFieldFocusGained
        textBoxWithFocus = Base.BINARY;
    }//GEN-LAST:event_binaryTextFieldFocusGained

    private void octalTextFieldFocusGained(java.awt.event.FocusEvent evt)//GEN-FIRST:event_octalTextFieldFocusGained
    {//GEN-HEADEREND:event_octalTextFieldFocusGained
        textBoxWithFocus = Base.OCTAL;
    }//GEN-LAST:event_octalTextFieldFocusGained

    private void decimalTextFieldFocusGained(java.awt.event.FocusEvent evt)//GEN-FIRST:event_decimalTextFieldFocusGained
    {//GEN-HEADEREND:event_decimalTextFieldFocusGained
        textBoxWithFocus = Base.DECIMAL;
    }//GEN-LAST:event_decimalTextFieldFocusGained

    private void hexTextFieldFocusGained(java.awt.event.FocusEvent evt)//GEN-FIRST:event_hexTextFieldFocusGained
    {//GEN-HEADEREND:event_hexTextFieldFocusGained
        textBoxWithFocus = Base.HEXADECIMAL;
    }//GEN-LAST:event_hexTextFieldFocusGained

    private void resetPreferencesMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_resetPreferencesMenuItemActionPerformed
    {//GEN-HEADEREND:event_resetPreferencesMenuItemActionPerformed
        try
        {
            preferences.clear();
        }
        catch (BackingStoreException ex)
        {
            Logger.getLogger(BaseConverterGUI.class.getName()).log(Level.SEVERE, "Error occured while resetting user preferences.", ex);
        }
        binaryPanel.setVisible(true);
        octalPanel.setVisible(true);
        decimalPanel.setVisible(true);
        hexPanel.setVisible(true);
        showBinaryCheckBoxMenuItem.setSelected(true);
        showDecimalCheckBoxMenuItem.setSelected(true);
        showOctalCheckBoxMenuItem.setSelected(true);
        showHexCheckBoxMenuItem.setSelected(true);
        
        setAlwaysOnTop(true);
        pinCheckBoxMenuItem.setSelected(true);
        
        Font font = new Font("Tahoma", Font.PLAIN, 13);
        binaryTextField.setFont(font);
        octalTextField.setFont(font);
        decimalTextField.setFont(font);
        hexTextField.setFont(font);
        pack();
    }//GEN-LAST:event_resetPreferencesMenuItemActionPerformed

    public enum Base
    {

        BINARY,
        OCTAL,
        DECIMAL,
        HEXADECIMAL
    }

    public void updateNumbers()
    {
        removeDocListeners();
        String notANumber = "Not a number";
        String numberString;
        switch (textBoxWithFocus)
        {
            case BINARY:
                // Remove spacers from binary text
                numberString = StringFormatter.removeSeparators(binaryTextField.getText(), "_");

                if (BaseConversionTools.isBinaryString(numberString))
                {
                    octalTextField.setText(BaseConversionTools.binaryToOctal(numberString));
                    decimalTextField.setText(BaseConversionTools.binaryToDecimal(numberString));
                    hexTextField.setText(BaseConversionTools.binaryToHexadecimal(numberString));
                }
                else
                {
                    octalTextField.setText(notANumber);
                    decimalTextField.setText(notANumber);
                    hexTextField.setText(notANumber);
                }
                break;
            case OCTAL:
                numberString = StringFormatter.removeSeparators(octalTextField.getText(), "_");

                if (BaseConversionTools.isOctalString(numberString))
                {
                    binaryTextField.setText(BaseConversionTools.octalToBinary(numberString));
                    decimalTextField.setText(BaseConversionTools.octalToDecimal(numberString));
                    hexTextField.setText(BaseConversionTools.octalToHexadecimal(numberString));
                }
                else
                {
                    binaryTextField.setText(notANumber);
                    decimalTextField.setText(notANumber);
                    hexTextField.setText(notANumber);
                }
                break;
            case DECIMAL:
                numberString = StringFormatter.removeSeparators(decimalTextField.getText(), "_");

                if (BaseConversionTools.isDecimalString(numberString))
                {
                    binaryTextField.setText(BaseConversionTools.decimalToBinary(numberString));
                    octalTextField.setText(BaseConversionTools.decimalToOctal(numberString));
                    hexTextField.setText(BaseConversionTools.decimalToHexadecimal(numberString));
                }
                else
                {
                    binaryTextField.setText(notANumber);
                    octalTextField.setText(notANumber);
                    hexTextField.setText(notANumber);
                }
                break;
            case HEXADECIMAL:
                numberString = StringFormatter.removeSeparators(hexTextField.getText(), "_");

                if (BaseConversionTools.isHexadecimalString(numberString))
                {
                    binaryTextField.setText(BaseConversionTools.hexadecimalToBinary(numberString));
                    octalTextField.setText(BaseConversionTools.hexadecimalToOctal(numberString));
                    decimalTextField.setText(BaseConversionTools.hexadecimalToDecimal(numberString));
                }
                else
                {
                    binaryTextField.setText(notANumber);
                    octalTextField.setText(notANumber);
                    decimalTextField.setText(notANumber);
                }
                break;
            default:
                break;
        }
        addDocListeners();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel binaryPanel;
    private javax.swing.JTextField binaryTextField;
    private javax.swing.JPanel decimalPanel;
    private javax.swing.JTextField decimalTextField;
    private javax.swing.JMenuItem fontMenuItem;
    private javax.swing.JFrame helpFrame;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JPanel hexPanel;
    private javax.swing.JTextField hexTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPanel layoutPanel;
    private javax.swing.JPanel octalPanel;
    private javax.swing.JTextField octalTextField;
    private javax.swing.JMenu optionsMenu;
    private javax.swing.JCheckBoxMenuItem pinCheckBoxMenuItem;
    private javax.swing.JMenuItem resetPreferencesMenuItem;
    private javax.swing.JCheckBoxMenuItem showBinaryCheckBoxMenuItem;
    private javax.swing.JCheckBoxMenuItem showDecimalCheckBoxMenuItem;
    private javax.swing.JCheckBoxMenuItem showHexCheckBoxMenuItem;
    private javax.swing.JCheckBoxMenuItem showOctalCheckBoxMenuItem;
    // End of variables declaration//GEN-END:variables
}
